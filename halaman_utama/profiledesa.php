<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Profile Desa Lohbener</title>

  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="css/stylish-font.css" rel="stylesheet" type="text/css">
  <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
  <link href="css/stylish-portfolio.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="style.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/w3.css">
</head>

<body id="page-top">
    	

  <!-- Navigasi -->
  <a class="menu-toggle rounded" href="#">
    <i class="fas fa-bars"></i>
  </a>
  <nav id="sidebar-wrapper">
    <ul class="sidebar-nav">
      <li class="sidebar-brand">
        <a class="js-scroll-trigger" href="#page-top">DESA LOHBENER</a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" href="index.php"><i class="fas fa-house-damage"></i> Home</a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" href="profiledesa.php"><i class="fa fa-university"></i> Profil Desa</a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" href="../login_desa/login.php"><i class="fas fa-sign-in-alt"></i> Login</a>
      </li>
       <li class="sidebar-nav-item"> 
        <a class="js-scroll-trigger" href="#contact"><i class="fa fa-map-marker"></i> Lihat Maps</a>
      </li>
    </ul>
  </nav>

  <!-- Header -->
  <header class="masthead d-flex">
    <div class="container text-center my-auto">
      <h1 class="mb-1"></h1>
      <h3 class="mb-5">
      </h3>
      <a class="btn btn-primary btn-xl js-scroll-trigger" href="#pemerintahan">Temukan struktur pemerintahan kami</a>
    </div>
    <div class="overlay"></div>
  </header>

    	<h3 style="text-align: center; font-family: Open sans;" id="pemerintahan">Pemerintahan Desa</h3>
    	<br>
    	
	<table id="main-table" class="table table-condensed">
		<thead>
			<tr>
				<th>No</th>
				<th>Jabatan</th>
				<th>Nama</th>
				<th>Pendidikan</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>1</td>
				<td>Kuwu</td>
				<td>H. Rakhmatu S</td>
				<td>Ijazah SMA Sederajat</td>
			</tr>
			<tr>
				<td>2</td>
				<td>Sekretaris Desa</td>
				<td>Al-Misbah</td>
				<td>Ijazah SMA Sederajat</td>
			</tr>
			<tr>
				<td>3</td>
				<td>Tata Usaha</td>
				<td>Ma'mun Fattah</td>
				<td>Ijazah D4 / S1</td>
			</tr>
			<tr>
				<td>4</td>
				<td>Bendahara</td>
				<td>Edi Junaedi</td>
				<td>Ijazah SMA Sederajat</td>
			</tr>
			<tr>
				<td>5</td>
				<td>Lurah</td>
				<td>Sunanto</td>
				<td>Ijazah SMA Sederajat</td>
			</tr>
			<tr>
				<td>6</td>
				<td>Raksabumi</td>
				<td>Casbin</td>
				<td>Ijazah SMA Sederajat</td>
			</tr>
			<tr>
				<td>7</td>
				<td>Kliwon</td>
				<td>Rahmat Fauzi</td>
				<td>Ijazah SMA Sederajat</td>
			</tr>
			<tr>
				<td>8</td>
				<td>Lebe</td>
				<td>Warka</td>
				<td>Ijazah SMA Sederajat</td>
			</tr>
			<tr>
				<td>9</td>
				<td>Bekel</td>
				<td>Turidi</td>
				<td>Ijazah SMA Sederajat</td>
			</tr>
			
		</tbody>
    </table>
    <hr></hr>

		<br>
		<br>
		<h4 align="center">Administrasi Desa</h4>
		<br>
		<table id="main-table" class="table table-condensed">
		<thead>
			<tr>
				<th>No</th>
				<th>Nama Wilayah Administrasi</th>
				<th>Satuan</th>
				<th>Jumlah</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>1</td>
				<td>Jumlah Dusun</td>
				<td>Dusun</td>
				<td>0</td>
			</tr>
			<tr>
				<td>2</td>
				<td>Jumlah Rukun Warga</td>
				<td>RW</td>
				<td>8</td>
			</tr>
			<tr>
				<td>3</td>
				<td>Jumlah Rukun Tetangga</td>
				<td>RT</td>
				<td>39</td>

			</tr>
			
		</tbody>	
    </table>
	
	<br>
	<br>
  
  
  <!-- Map -->
  <section id="contact" class="map">
    <iframe width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7929.9189324231365!2d108.28379407152818!3d-6.39922489177385!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1ada0c29ffe9a792!2sKantor%20Kuwu%20Lohbener!5e0!3m2!1sid!2sid!4v1573801151711!5m2!1sid!2sid"></iframe>
    <br />
    <small>
      <a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;aq=0&amp;oq=twitter&amp;sll=28.659344,-81.187888&amp;sspn=0.128789,0.264187&amp;ie=UTF8&amp;hq=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;t=m&amp;z=15&amp;iwloc=A"></a>
    </small>
  </section>

  <!-- Footer -->
  <footer class="footer text-center">
    <div class="container">
      
      <p class="text-muted small mb-0">Copyright &copy; Desa Lohbener, Indramayu</p>
    </div>
  </footer>

  
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="js/stylish-portfolio.min.js"></script>
  <script src="../assets/js/sweetalerts.js"></script>

</body>

</html>
