<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Desa Lohbener</title>

  <link href="../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="../assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="../assets/css/stylish-font.css" rel="stylesheet" type="text/css">
  <link href="../assets/vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
  <link href="../assets/css/stylish-portfolio.css" rel="stylesheet">
  <link href="../assets/css/style.css" rel="stylesheet" type="text/css">
  <link href="../assets/css/w3.css" rel="stylesheet" type="text/css">

</head>

<body id="page-top">

  <!-- Navigasi -->
  <a class="menu-toggle rounded" href="#">
    <i class="fas fa-bars"></i>
  </a>
  <nav id="sidebar-wrapper">
    <ul class="sidebar-nav">
      <li class="sidebar-brand">
        <a class="js-scroll-trigger" href="#page-top">DESA LOHBENER</a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" href="#page-top"><i class="fas fa-house-damage"></i> Home</a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" href="profiledesa.php"><i class="fa fa-university"></i> Profil Desa</a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" href="../login_desa/login.php"><i class="fas fa-sign-in-alt"></i> Login</a>
      </li>
    </ul>
  </nav>

  <!-- Header -->
  <header class="masthead d-flex">
    <div class="container text-center my-auto">
      <h1 class="mb-1"></h1>
      <h3 class="mb-5">
      </h3>
      <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Temukan kami lebih dalam</a>
    </div>
    <div class="overlay"></div>
  </header>

  <!-- Tentang -->
  <section class="content-section bg-light" id="about">
    <div class="container text-center">
      <div class="row">
        <div class="col-lg-10 mx-auto">
          <h2>Sebuah desa yang termasuk kedalam Kecamatan Lohbener, Kabupaten Indramayu, Jawa Barat</h2>
          <a class="btn btn-dark btn-xl js-scroll-trigger" href="#services">Apa yang dapat kami lakukan</a>
        </div>
      </div>
    </div>
  </section>

  <!-- Hal-hal -->
  <section class="content-section bg-primary text-white text-center" id="services">
    <div class="container">
      <div class="content-section-heading">
        <h2 class="mb-5">Pembuatan Surat Pengantar</h2>
      </div>
      <div class="row" style="margin-left: 210px;">
        <div class="col-lg-3 col-md-6 mb-5 mb-lg-0">
          <span class="service-icon rounded-circle mx-auto mb-3">
            <i class="fa fa-id-card"></i>
          </span>
          <h4>
            <strong><a href="../login_desa/login.php">Kartu Tanda Penduduk</a></strong>
          </h4>
        </div>
        <div class="col-lg-3 col-md-6 mb-5 mb-lg-0">
          <span class="service-icon rounded-circle mx-auto mb-3">
            <i class="icon-pencil"></i>
          </span>
          <h4>
            <strong>Surat Keterangan Tidak Mampu</strong>
          </h4>
        </div>
        <div class="col-lg-3 col-md-6 mb-5 mb-md-0">
          <span class="service-icon rounded-circle mx-auto mb-3">
            <i class="icon-like"></i>
          </span>
          <h4>
           <strong>Surat Keterangan Catatan Kepolisian</strong>
         </h4>
       </div>
     </div>
   </div>
 </section>
 
 <!-- Footer -->
 <footer class="footer text-center">
  <div class="container">
    <p class="text-muted small mb-0">Copyright &copy; Desa Lohbener, Indramayu</p>
  </div>
</footer>

<a class="scroll-to-top rounded js-scroll-trigger" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>

<script src="../assets/vendor/jquery/jquery.min.js"></script>
<script src="../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../assets/vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="../assets/js/stylish-portfolio.min.js"></script>
<script src="../asset/js/sweetalerts.js"></script>
</body>

</html>
