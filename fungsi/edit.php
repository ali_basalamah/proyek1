<?php

include_once("config.php");


if(isset($_POST['update_data']))
{   
    $nik                = $_GET['nik'];
    $nama_lengkap       = $_POST['nama_lengkap'];
    $nama_panggilan     = $_POST['nama_panggilan'];
    $pekerjaan          = $_POST['pekerjaan'];
    $agama              = $_POST['agama'];
    $ttl                = $_POST['tanggal_lahir'];
    $alamat_lengkap     = $_POST['alamat_lengkap'];
    $jk                 = $_POST['jk'];

   
    mysqli_query($koneksi, "UPDATE data_penduduk SET nik ='$nik', nama_lengkap = '$nama_lengkap', nama_panggilan = '$nama_panggilan', pekerjaan = '$pekerjaan', agama = '$agama', tanggal_lahir = '$ttl', alamat_lengkap = '$alamat_lengkap', jk = '$jk' WHERE nik= $nik");

    header("Location: crud_utama.php");
}

$nik            = "";
$nama_lengkap   = "";
$nama_panggilan = "";
$pekerjaan      = "";
$agama          = "";
$ttl            = "";
$alamat_lengkap = "";
$jk             = "";

?>

<?php

$nik = $_GET['nik'];

$masuk = mysqli_query($koneksi, "SELECT * FROM data_penduduk WHERE nik = $nik");


while($data_user  = mysqli_fetch_array($masuk))
{
    $nik                = $data_user['nik'];
    $nama_lengkap       = $data_user['nama_lengkap'];
    $nama_panggilan     = $data_user['nama_panggilan'];
    $pekerjaan          = $data_user['pekerjaan'];
    $agama              = $data_user['agama'];
    $ttl                = $data_user['tanggal_lahir'];
    $alamat_lengkap     = $data_user['alamat_lengkap'];
    $jk                 = $data_user['jk'];

}

?>
<html>
<head>  
    <title>Edit Data</title>
</head>

<body>
    <a href="crud_utama.php">Halaman Utama Data</a>
    <br/><br/>

    <form name="update_data" method="post" action="edit.php?nik=<?php echo $nik; ?>">
        <table border="0">
            <tr> 
                <td>NIK</td>
                <td><input type="text" name="nik" value="<?php echo $nik;?>"></td>
            </tr>
            <tr> 
                <td>Nama Lengkap</td>
                <td><input type="text" name="nama_lengkap" value="<?php echo $nama_lengkap;?>"></td>
            </tr>
            <tr> 
                <td>Nama Panggilan</td>
                <td><input type="text" name="nama_panggilan" value="<?php echo $nama_panggilan;?>"></td>
            </tr>
            <tr> 
                <td>Pekerjaan</td>
                <td><input type="text" name="pekerjaan" value="<?php echo $pekerjaan;?>"></td>
            </tr>
            <tr> 
                <td>Agama</td>
                <td><input type="text" name="agama" value="<?php echo $agama;?>"></td>
            </tr>
            <tr> 
                <td>Tanggal Lahir</td>
                <td><input type="text" name="tanggal_lahir" value="<?php echo $ttl;?>"></td>
            </tr>
            <tr> 
                <td>Alamat Lengkap</td>
                <td><input type="text" name="alamat_lengkap" value="<?php echo $alamat_lengkap;?>"></td>
            </tr>
            <tr> 
                <td>Jenis Kelamin</td>
                <td><input type="text" name="jk" value="<?php echo $jk;?>"></td>
            </tr>
        
            <tr>
                <td><input type="submit" name="update_data" value="Update"></td>
            </tr>
        </table>
    </form>
</body>
</html>