<?php

include_once("config.php");

$urut = mysqli_query($koneksi, "SELECT * FROM data_penduduk ORDER BY nama_lengkap ASC");

?>

<?php

session_start();

$nama_lengkap = $_SESSION['nama_lengkap'];
if ($nama_lengkap)
{
    
}else
{
    header("location: login.php");
}

?>

<DOCTYPE html>
    <head>
        <link rel="stylesheet" type="text/css" href="../halaman_utama/style.css">
        <meta charset='utf-8'>
        <meta http-equiv='X-UA-Compatible' content='IE=edge'>
        <title>Halaman Utama</title>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <link rel="stylesheet"  href="../assets/css/bootstrap.css">

        <script src="../assets/js/sweetalerts.js"></script>
        <script src="../assets/js/bootstrap.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/jquery.min.js"></script>
        <script type="text/javascript">

            alert(<?php echo "Selamat Datang ".$_SESSION['nama_lengkap']; ?>);
            
        </script>  
        <title>Halaman Data</title>
    </head>

    <body>

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">

              <ul class="navbar-nav mr-auto">

              </ul>

              <span class="navbar-text mr-3">
                Silahkan logout jika telah selesai
            </span>

            <a href="#" class="btn btn-outline-success mr-2">Logout</a>

        </div>
    </nav>
    <div class="header">
        <h1>DESA LOHBENER</h1>
        <h3>SELAMAT DATANG DI WEB DESA LOHBENER</h3>
    </div>
    <h1 style="text-align: center; margin-top: 50px;">DATA PENDUDUK DESA LOHBENER</h1>
    <table width = '80%' border = 1.5 style="margin-left: 130px; margin-top: 30px;">

        <a href="../login_desa/daftarakun.php" style="margin-left: 50px;">(+) Tambah Data</a><br>

        <br>
        <tr>
            <th>NIK</th> <th>Nama Lengkap</th> <th>Nama Panggilan</th> <th>Pekerjaan</th> <th>Agama</th> <th>Tanggal Lahir</th> <th>Alamat Lengkap</th> <th>Jenis Kelamin</th>
        </tr>

        <?php 

        while($data_user = mysqli_fetch_array($urut))
        {         
            echo "<tr>";
            echo "<td>".$data_user['nik']."</td>";
            echo "<td>".$data_user['nama_lengkap']."</td>";
            echo "<td>".$data_user['nama_panggilan']."</td>";
            echo "<td>".$data_user['pekerjaan']."</td>";
            echo "<td>".$data_user['agama']."</td>";
            echo "<td>".$data_user['tanggal_lahir']."</td>";
            echo "<td>".$data_user['alamat_lengkap']."</td>";
            echo "<td>".$data_user['jk']."</td>";
            echo "<td><a href='edit.php?nik=$data_user[nik]'>Edit</a> | <a href='hapus.php?nik=$data_user[nik]'>Hapus</a></td></tr>";        
        }

        ?>
    </table>
</body>
</html>
