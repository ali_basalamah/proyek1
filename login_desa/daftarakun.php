<!DOCTYPE html>
<html>
<head>
	<title>Form Registrasi</title>
	<link rel="stylesheet" type="text/css" href="css/regis.css">
	<script type="text/javascript">

		//alert("Selamat Datang Di Halaman Registrasi");
	
	</script>
</head>
<body>

	<div class="back-kotakreg" style="background-image: url('images/desalohbener1.jpg');">
	
		<div class="kotak_regis">
			<p class="tulisan_regis"><b>Silahkan Registrasi</p></b>

			<form name="form_daftar" method="POST" action="proses_daftar.php">

					<label><b>Nomor Induk Kependudukan</b></label>
					<input type="text" name="nik" class="form_regis" placeholder="NIK" required="on" autocomplete="off">
					<br>
					<br>
				
					<label><b>Nama Lengkap</b></label>
					<input type="text" name="nama_lengkap" class="form_regis" placeholder="Nama Lengkap" required="on" autocomplete="off">
					<br>
					<br>

					<label><b>Nama Panggilan</b></label>
					<input type="text" name="nama_panggilan" class="form_regis" placeholder="Nama Panggilan" required="on" autocomplete="off">
					<br>
					<br>

					<label><b>Pekerjaan</b></label>
					<input type="text" name="pekerjaan" class="form_regis" placeholder="Pekerjaan" required="on" autocomplete="off">
					<br>
					<br>

					<label><b>Agama</b></label>
					<input type="text" name="agama" class="form_regis" placeholder="Agama" required="on" autocomplete="off">
					<br>
					<br>


					<label><b>Tanggal Lahir</b></label>
					<input type="date" name="ttl" value="tgl_lahir" class="form_regis" required="on" autocomplete="off">
					<br>
					<br>

					<label><b>Alamat Lengkap</b></label>
					<textarea class="form_regis" name="alamat_lengkap" required="on" autocomplete="off"></textarea>

					<br>
					<br>

					<label><b>Jenis Kelamin</b></label>
					<br>
					<input type="radio" name="jk" value="pria" required="on" style="margin-top: 8px;">Pria
					<br>
					<input type="radio" name="jk" value="wanita" required="on">Wanita

					<br>
					<br>
					<br>

					<label><b>Password</b></label>
					<input type="password" name="password" class="form_regis" placeholder="Password" required="on" autocomplete="off">
					
					<button type="submit" name="form_daftar" class="tombol_regis">DAFTAR AKUN</button>

			</form>
		
		</div>

	</div>
</body>
</html>

