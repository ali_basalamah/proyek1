<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Halaman Login</title>

	<link href="../assets/css/login.css" rel="stylesheet" type="text/css">
	<link href="../assets/css/stylish-portfolio.css" rel="stylesheet">
	<link href="../assets/css/stylish-font.css" rel="stylesheet" type="text/css">
	<link href="../assets/css/w3.css" rel="stylesheet" type="text/css">
	<link href="../assets/vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
	<link href="../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="../assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<script src="../assets/js/sweetalerts.js"></script>

</head>

<body id="page-top">

	<script type="text/javascript">
		Swal.fire({
			showCloseButton:true,
			type: 'success',
			title: 'Selamat Datang',
			text: 'Silahkan Login Untuk Membuat Surat Pengantar',
		})
	</script>

	<a class="menu-toggle rounded" href="#">
		<i class="fas fa-bars"></i>
	</a>
	<nav id="sidebar-wrapper">
		<ul class="sidebar-nav">
			<li class="sidebar-brand">
				<a class="js-scroll-trigger" href="#page-top">DESA LOHBENER</a>
			</li>
			<li class="sidebar-nav-item">
				<a class="js-scroll-trigger" href="../halaman_utama/index.php"><i class="fas fa-house-damage"></i> Home</a>
			</li>
			<li class="sidebar-nav-item">
				<a class="js-scroll-trigger" href="../halaman_utama/profiledesa.php"><i class="fa fa-university"></i> Profil Desa</a>
			</li>
			<li class="sidebar-nav-item">
				<a class="js-scroll-trigger" href="login.php"><i class="fas fa-sign-in-alt"></i> Login</a>
			</li>
		</ul>
	</nav>

	<div class="back-kotaklog">
		
		<div class="kotak_login">
			<p class="tulisan_login"><b>Silahkan login</p></b>

			<form action="validasi.php" name="form_desa" method="POST">
				<fieldset>
					
					<br>
					<label>Username</label>
					<input type="text" name="nama_lengkap" class="form_login" placeholder="Nama Lengkap" required="on" autocomplete="off">
					<br>
					<br>

					<label>Password</label>
					<br>
					<input type="password" name="password" class="form_login" placeholder="Password" required="on" autocomplete="off">
					<br>
					
					<button type="submit" class="tombol_login">MASUK</button>
					<br>
				</fieldset>
			</form>
			
		</div>

	</div>
	
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
	<script src="js/stylish-portfolio.min.js"></script>

</body>
</html>