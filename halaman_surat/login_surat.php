<!DOCTYPE html>
<html>
<head>
    <title>BIODATA DIRI</title>
    <link rel="stylesheet" type="text/css" href="../login_desa/css/login.css">
    <?php 

            session_start();
 
    // cek apakah yang mengakses halaman ini sudah login
            if($_SESSION['level']=="")
            {
                header("location: ../login_desa/login.php");
            }
 
    ?>
</head>
<body>
    <h1 style="text-align: center;">BIODATA DIRI</h1>

        <table style="margin-top: 1px; margin-bottom: 50px;" width="1000" border="1" cellspacing="0" cellpadding="5" align="center">
            <tr align="center" bgcolor="#66CC33">
                <td width="174"><b>DATA DIRI</b></td>
                <td width="353"><b>KETERANGAN</b></td>
            </tr>

            <tr>
                <td>Nama Lengkap</td>
                <td><input type="text" name="nama" autocomplete="off" class="form_login" required="on"></td>
            </tr>

            <tr>
                <td>Agama</td>
                <td><input type="text" name="agama" autocomplete="off" class="form_login" required="on"></td>
            </tr>

            <tr>
                <td>Jenis Kelamin</td>
                <td><input type="radio" name="jk" value="pria" style="margin-top: 8px;">Pria<br><input type="radio" name="jk" value="wanita">Wanita</td>

            </tr>

            <tr>
                <td>Status</td>
                <td><input type="text" name="status" autocomplete="off" class="form_login" required="on"></td>
            </tr>

            <tr>
                <td>Pekerjaan</td>
                <td><input type="text" name="pekerjaan" autocomplete="off" class="form_login" required="on"></td>
            </tr>

            <tr>
                <td>Tempat, Tanggal Lahir</td>
                <td><input type="text" name="ttl" autocomplete="off" class="form_login" required="on"></td>
            </tr>

            <tr>
                <td>Alamat</td>
                <td><textarea name="alamat" class="form_login"></textarea></td>
            </tr>

        </table>
</body>
</html>